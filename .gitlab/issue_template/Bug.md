## Descripción del Bug

**Descripción:**
Una breve descripción del problema.

**Pasos para reproducir:**
1. Paso detallado 1.
2. Paso detallado 2.
3. Paso detallado 3.
4. ...

**Comportamiento Esperado:**
Describe lo que debería ocurrir si el bug no existiera.

**Comportamiento Actual:**
Describe lo que realmente está ocurriendo.

## Información Adicional

**Ambiente:**
- **Sistema Operativo:** (Ej. Windows 10, macOS 12.1, Ubuntu 20.04)
- **Navegador/Versión:** (Ej. Chrome 91, Firefox 89)
- **Versión de la Aplicación:** (Ej. 1.0.0)
- **Hardware:** (Opcional, si el bug depende del hardware)

**Archivos Adjuntos:**
- Capturas de pantalla.
- Archivos de registro.
- Otros archivos relevantes.

## Notas

**Notas Adicionales:**
Cualquier otra información que pueda ser relevante para resolver el problema.

**Posibles Soluciones:**
´´´
# Especifica la imagen de Docker que se usará en las fases
image: node:20.13.1

# Define las etapas del pipeline
stages:
  - install
  - test
  - build
  - deploy

# Fase de instalación
install:
  stage: install
  script:
    - echo "Installing dependencies..."
    - npm ci  # Instala las dependencias especificadas en package-lock.json
  artifacts:
    paths:
      - node_modules/  # Guarda la carpeta node_modules como un artefacto para las siguientes fases

# Fase de pruebas
test:
  stage: test
  script:
    - echo "Running tests..."
    - npm run test  # Ejecuta el script de pruebas definido en package.json
  artifacts:
    reports:
      junit: junit.xml  # Genera un reporte en formato JUnit para integraciones con herramientas de CI/CD
    paths:
      - coverage/  # Guarda la carpeta de cobertura de pruebas como un artefacto

# Fase de construcción
build:
  stage: build
  script:
    - echo "Building the project..."
    - npm run build  # Ejecuta el script de construcción definido en package.json
  artifacts:
    paths:
      - dist/  # Guarda la carpeta dist (directorio de distribución) como un artefacto para la fase de despliegue

# Fase de despliegue en GitLab Pages
pages:
  stage: deploy
  script:
    - echo "Deploying to GitLab Pages..."
    - mkdir -p public  # Asegúrate de que la carpeta public existe
    - mv dist/* public  # Mueve todos los archivos de dist a public
    - echo '[[redirects]]' > public/_redirects
    - echo '  from = "/*"' >> public/_redirects
    - echo '  to = "/index.html"  200' >> public/_redirects
    - cat public/_redirects  # Verifica el contenido del archivo _redirects
  artifacts:
    paths:
      - public  # Guarda la carpeta public como el artefacto final para GitLab Pages
  only:
    - main  # Solo ejecuta el despliegue cuando se hace un push a la rama main
´´´

## Seguimiento

**Responsable:**
La persona asignada para resolver este bug.

**Estado:**
- [ ] Abierto
- [ ] En progreso
- [ ] Resuelto
- [ ] Cerrado